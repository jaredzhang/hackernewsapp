package com.jaredzhang.hackersnewsapp.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.jaredzhang.hackersnewsapp.BaseActivity;
import com.jaredzhang.hackersnewsapp.R;
import com.jaredzhang.hackersnewsapp.adapter.TopStoriesListAdapter;
import com.jaredzhang.hackersnewsapp.event.GetTopStoriesEvent;
import com.jaredzhang.hackersnewsapp.helper.CacheManager;
import com.jaredzhang.hackersnewsapp.helper.Const;
import com.jaredzhang.hackersnewsapp.helper.Utils;
import com.jaredzhang.hackersnewsapp.loader.ListLoader;
import com.jaredzhang.hackersnewsapp.model.Item;
import com.jaredzhang.hackersnewsapp.network.ApiConfig;
import com.jaredzhang.hackersnewsapp.service.UpdateTopStoriesService;
import com.jaredzhang.hackersnewsapp.widget.CustomSwipeRefreshLayout;
import com.jaredzhang.hackersnewsapp.widget.PagingRecyclerView;
import com.jaredzhang.hackersnewsapp.widget.Pagingnable;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;


public class TopStoriesListActivity extends BaseActivity implements TopStoriesListAdapter.OnItemClickListener, LoaderManager.LoaderCallbacks<List<Item>> {

    @InjectView(android.R.id.list)
    PagingRecyclerView mList;
    @InjectView(android.R.id.empty)
    TextView mEmpty;
    @InjectView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;
    @InjectView(R.id.toolbar)
    Toolbar mToolbar;

    TopStoriesListAdapter mAdapter;

    public static final int PAGE_SIZE = 6;

    int pageNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_stories);
        ButterKnife.inject(this);
        setSupportActionBar(mToolbar);

        initViews();

    }

    private void initViews() {
        mRefreshLayout.setTarget(mList);
        mList.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(mLayoutManager);
        mList.setHasMoreItems(true);
        mList.setPagingnableListener(new Pagingnable() {
            @Override
            public void onLoadMoreItems() {
                pageNo++;
                loadNextPage(false);

            }
        });
        mAdapter = new TopStoriesListAdapter();
        mAdapter.setOnItemClickListener(this);
        mList.setAdapter(mAdapter);
        mList.setIsLoading(true);
        pageNo = 1;
        loadNextPage(false);
        getSupportLoaderManager().initLoader(Const.TOP_STORY_LOADER_ID, null, this);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                loadNextPage(true);
            }
        });
    }

    private void loadNextPage(boolean refresh) {
        Intent intent = new Intent(TopStoriesListActivity.this, UpdateTopStoriesService.class);
        intent.putExtra(Const.INTENT_EXTRA_PAGE_NO, pageNo);
        intent.putExtra(Const.INTENT_EXTRA_PAGE_SIZE, PAGE_SIZE);
        intent.putExtra(Const.INTENT_EXTRA_REFRESH, refresh);
        startService(intent);
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    /*
    event bus callback
     */
    public void onEventMainThread(GetTopStoriesEvent event) {
        Timber.d(" top stories event status %s ", event.getStatus());
        if (event.isSucceed()) {
            if (mList != null) mList.setIsLoading(false);
            if (loader != null) {
                loader.setPageNo(pageNo);
            }
        }
        if (mRefreshLayout != null) mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(View view, int position, Item item) {
        Intent intent = new Intent(this, CommentsListActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_ITEM, (Parcelable) item);
        startActivity(intent);
    }

    @Override
    public void onViewOnWebClicked(View view, int position, Item item) {
        Utils.viewWeb(this, item.getUrl());
    }


    static class TopStoriesListLoader extends ListLoader<Item> {
        int pageNo;
        int pageSize;
        boolean hasMoreItems;
        CacheManager cacheManager = CacheManager.getInstance();

        public TopStoriesListLoader(Context ctx, int pageNo, int pageSize) {
            super(ctx);
            this.pageNo = pageNo;
            this.pageSize = pageSize;
        }

        public void setPageNo(int pageNo) {
            this.pageNo = pageNo;
            forceLoad();
        }

        public boolean isHasMoreItems() {
            return hasMoreItems;
        }

        @Override
        public List<Item> loadInBackground() {
            String topStoryCacheKey = Utils.md5(ApiConfig.EndPoint.TOP_STORIES);
            Integer[] ids = cacheManager.getArray(topStoryCacheKey, Integer.class);
            final List<Item> topItems = new ArrayList<>();
            if (ids == null) {
                hasMoreItems = true;
                return topItems;
            }

            //if size equals zero, return all
            if (pageSize == 0 || pageNo == 0) {
                pageNo = 1;
                pageSize = ids.length;
            }

            String tempCacheKey;
            Item tempItem;
            hasMoreItems = (pageSize * pageNo < ids.length);

            for (int i = 0; i < Math.min(pageSize * pageNo, ids.length); i++) {
                tempCacheKey = CacheManager.getItemCacheKey(ids[i]);
                tempItem = cacheManager.get(tempCacheKey, Item.class);
                if (tempItem != null) {
                    topItems.add(tempItem);
                }

            }

            return topItems;
        }

    }

    /*
    loader Callbacks
     */

    TopStoriesListLoader loader;

    @Override
    public Loader<List<Item>> onCreateLoader(int id, Bundle args) {
        loader = new TopStoriesListLoader(this, pageNo, PAGE_SIZE);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Item>> loader, List<Item> data) {
        mAdapter.setItems(data);
        if (mList != null) {
            mList.setHasMoreItems(((TopStoriesListLoader) loader).hasMoreItems);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Item>> loader) {
        mAdapter.setItems(null);
    }
}
