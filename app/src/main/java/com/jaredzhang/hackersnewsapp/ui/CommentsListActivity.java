package com.jaredzhang.hackersnewsapp.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jaredzhang.hackersnewsapp.BaseActivity;
import com.jaredzhang.hackersnewsapp.R;
import com.jaredzhang.hackersnewsapp.adapter.CommentsListAdapter;
import com.jaredzhang.hackersnewsapp.event.GetCommentsEvent;
import com.jaredzhang.hackersnewsapp.helper.CacheManager;
import com.jaredzhang.hackersnewsapp.helper.Const;
import com.jaredzhang.hackersnewsapp.helper.Utils;
import com.jaredzhang.hackersnewsapp.loader.ListLoader;
import com.jaredzhang.hackersnewsapp.model.Item;
import com.jaredzhang.hackersnewsapp.service.GetStoryCommentService;
import com.jaredzhang.hackersnewsapp.widget.CustomSwipeRefreshLayout;
import com.jaredzhang.hackersnewsapp.widget.PagingRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;


public class CommentsListActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<Item>> {

    @InjectView(android.R.id.list)
    PagingRecyclerView mList;
    @InjectView(android.R.id.empty)
    TextView mEmpty;
    @InjectView(R.id.refresh_layout)
    CustomSwipeRefreshLayout mRefreshLayout;

    Item parentItem;

    CommentsListAdapter mAdapter;

    public static final int PAGE_SIZE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        parentItem = getIntent().getParcelableExtra(Const.INTENT_EXTRA_ITEM);
        if (parentItem == null) {
            finish();
            return;
        }

        ButterKnife.inject(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        initViews();

        getSupportLoaderManager().initLoader(parentItem.getId(), null, this);
        refreshData(false);

    }

    private void initViews() {
        mRefreshLayout.setTarget(mList);
        mList.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mList.setLayoutManager(mLayoutManager);
        mList.setHasMoreItems(true);

        mAdapter = new CommentsListAdapter();
        mList.setAdapter(mAdapter);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData(true);
            }
        });
    }

    private void refreshData(boolean refresh) {
        Intent intent = new Intent(CommentsListActivity.this, GetStoryCommentService.class);
        intent.putExtra(Const.INTENT_EXTRA_PAGE_NO, 1);
        intent.putExtra(Const.INTENT_EXTRA_PAGE_SIZE, PAGE_SIZE);
        intent.putExtra(Const.INTENT_EXTRA_REFRESH, refresh);
        intent.putExtra(Const.INTENT_EXTRA_ITEM_ID, parentItem.getId());
        intent.putExtra(Const.INTENT_EXTRA_COMMENT_IDS, parentItem.getKids());
        startService(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comments, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view) {
            Utils.viewWeb(this, parentItem.getUrl());
            return true;
        } else if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    /*
    event bus callback
     */
    public void onEventMainThread(GetCommentsEvent event) {
        Timber.d(" comment event status %s ", event.getStatus());
        if (event.isSucceed()) {
            if (loader != null) loader.forceLoad();
            //mAdapter.setItems(event.getResponse());
        }
        if (mRefreshLayout != null) mRefreshLayout.setRefreshing(false);
    }

    static class CommentListLoader extends ListLoader<Item> {
        Item parentItem;
        CacheManager cacheManager = CacheManager.getInstance();

        public CommentListLoader(Context ctx, Item parentItem) {
            super(ctx);
            this.parentItem = parentItem;
        }

        @Override
        public List<Item> loadInBackground() {

            final List<Item> commentItems = new ArrayList<>();

            if (parentItem.getKids() == null || parentItem.getKids().length == 0)
                return commentItems;

            int[] commentIds = parentItem.getKids();
            String tempCacheKey;
            Item tempItem;
            for (int i = 0; i < Math.min(commentIds.length, PAGE_SIZE); i++) {
                tempCacheKey = CacheManager.getCommentCacheKey(commentIds[i]);
                tempItem = cacheManager.get(tempCacheKey, Item.class);
                if (tempItem != null) commentItems.add(tempItem);
            }

            return commentItems;
        }
    }

    /*
    loader Callbacks
     */

    Loader<List<Item>> loader;

    @Override
    public Loader<List<Item>> onCreateLoader(int id, Bundle args) {
        loader = new CommentListLoader(this, parentItem);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Item>> loader, List<Item> data) {
        mAdapter.setItems(data);
        if (mList != null) {
            int size = (data == null?0:data.size());
            boolean more = (size < Math.min(PAGE_SIZE,parentItem.getKids()== null? 0:parentItem.getKids().length));
            mList.setHasMoreItems(more);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Item>> loader) {
        mAdapter.setItems(null);
    }


}
