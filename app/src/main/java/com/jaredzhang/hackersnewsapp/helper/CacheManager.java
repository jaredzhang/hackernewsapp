package com.jaredzhang.hackersnewsapp.helper;

import com.jaredzhang.hackersnewsapp.HackerNewsApplication;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import timber.log.Timber;

/**
 * Created by zhuodong on 6/4/14.
 */
public class CacheManager {

    private static CacheManager ourInstance = new CacheManager();

    public static CacheManager getInstance() {
        return ourInstance;
    }

    DB snappydb;

    private CacheManager() {
        try {
            checkDB();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    private void checkDB() throws SnappydbException {
        if (snappydb == null || !snappydb.isOpen()) {
            snappydb = DBFactory.open(HackerNewsApplication.getInstance());
        }
    }

    public <T> T get(String key, Class<T> returnType) {
        try {
            checkDB();
            if (snappydb == null || !snappydb.exists(key)) {
                return null;
            } else {
                return snappydb.getObject(key, returnType);
            }

        } catch (SnappydbException e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T> T[] getArray(String key, Class<T> returnType) {
        try {
            checkDB();
            if (snappydb == null || !snappydb.exists(key)) {
                return null;
            } else {
                return snappydb.getObjectArray(key, returnType);
            }

        } catch (SnappydbException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void put(String key, Object[] value) {
        try {
            checkDB();

            if (snappydb != null) {
                snappydb.put(key, value);
                Timber.d("", " save data for baseKey %s", key);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void put(String key, Object value) {
        try {
            checkDB();

            if (snappydb != null) {
                snappydb.put(key, value);
                Timber.d("", " save data for baseKey %s", key);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void clear(String key) {
        try {
            checkDB();

            if (snappydb == null) return;
            snappydb.del(key);

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void clearAll() {
        try {
            checkDB();
            if (snappydb != null) {
                snappydb.destroy();
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public static String getItemCacheKey(int id) {
        return ITEM_BASE_KEY + id;
    }

    public static String getCommentCacheKey(int id) {
        return COMMENTS_BASE_KEY + id;
    }


    public static final String COMMENTS_BASE_KEY = "comment:";

    public static final String ITEM_BASE_KEY = "item:";

    public void delete(String baseKey) {
        try {
            checkDB();

            if (snappydb == null) return;

            for (String key : snappydb.findKeys(baseKey)) {
                snappydb.del(key);
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

}
