package com.jaredzhang.hackersnewsapp.helper;

import android.content.SharedPreferences;

import com.jaredzhang.hackersnewsapp.HackerNewsApplication;

/**
 * Created by zhuodong on 6/4/14.
 */
public class LastUpdateTimeDataHelper {


    static SharedPreferences sharedPreferences = HackerNewsApplication.getInstance().getLastUpdatePref();

    private LastUpdateTimeDataHelper() {
    }

    public static long getTimeStamp(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public static void saveTimeStamp(String key) {
        sharedPreferences.edit().putLong(key, System.currentTimeMillis()).apply();
    }

    public static boolean isExpired(String key) {
        return System.currentTimeMillis() - getTimeStamp(key) > Const.UPDATE_PERIOD;
    }


    public static void clearAll() {
        sharedPreferences.edit().clear().apply();
    }

    public static void clear(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

}
