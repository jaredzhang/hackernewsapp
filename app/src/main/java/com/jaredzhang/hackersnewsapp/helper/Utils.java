package com.jaredzhang.hackersnewsapp.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Created by zhuodong on 2/7/15.
 */
public class Utils {
    private Utils() {

    }

    public static String md5(String token) {
        try {
            byte[] plainText = token.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(plainText);
            byte[] digest = md.digest();
            BigInteger bitInt = new BigInteger(1, digest);
            return bitInt.toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void viewWeb(Context context, String url) {
        if(TextUtils.isEmpty(url))return;
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
        if(intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}
