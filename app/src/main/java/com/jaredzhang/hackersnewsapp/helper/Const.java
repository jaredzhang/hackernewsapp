package com.jaredzhang.hackersnewsapp.helper;

/**
 * Created by zhuodong on 2/7/15.
 */
public class Const {
    // for preference key
    public static final String LAST_UPDATE_PREF = "last_update_pref";

    public static final String INTENT_EXTRA_PAGE_SIZE = "extra_page_size";
    public static final String INTENT_EXTRA_PAGE_NO = "extra_page_no";
    public static final String INTENT_EXTRA_REFRESH = "extra_refresh";
    public static final String INTENT_EXTRA_COMMENT_IDS = "extra_comment_ids";
    public static final String INTENT_EXTRA_ITEM_ID = "extra_item_id";


    public static final int UPDATE_PERIOD = 300000; // 5 mins

    public static final String INTENT_EXTRA_ITEM = "extra_item";
    public static final int TOP_STORY_LOADER_ID = 1;
}
