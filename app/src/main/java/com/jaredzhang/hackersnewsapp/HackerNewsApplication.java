package com.jaredzhang.hackersnewsapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.jaredzhang.hackersnewsapp.helper.Const;

import timber.log.Timber;

import static android.content.pm.PackageManager.NameNotFoundException;

/**
 * Created by zhuodong on 4/9/14.
 */

public class HackerNewsApplication extends Application {

    private static HackerNewsApplication instance;

    public static String versionName;

    public static int versionCode;

    public static SharedPreferences mSharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }

        instance = this;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            versionName = getPackageManager().getPackageInfo(
                this.getPackageName(), 0).versionName;
            versionCode = getPackageManager().getPackageInfo(
                this.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static HackerNewsApplication getInstance() {
        return instance;
    }


    public SharedPreferences getLastUpdatePref() {
        return getSharedPreferences(Const.LAST_UPDATE_PREF, 0);
    }

    /** A tree which logs important information for crash reporting. */
    private static class CrashReportingTree extends Timber.HollowTree {

        @Override
        public void i(String message, Object... args) {
            // TODO e.g., Crashlytics.log(String.format(message, args));
        }

        @Override
        public void i(Throwable t, String message, Object... args) {
            i(message, args); // Just add to the log.
        }

        @Override
        public void e(String message, Object... args) {
            i("ERROR: " + message, args); // Just add to the log.
        }

        @Override
        public void e(Throwable t, String message, Object... args) {
            e(message, args);

            // TODO e.g., Crashlytics.logException(t);
        }
    }

}
