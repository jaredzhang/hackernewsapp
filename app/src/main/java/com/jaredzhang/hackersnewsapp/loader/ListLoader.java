package com.jaredzhang.hackersnewsapp.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.List;

/**
 * An implementation of AsyncTaskLoader which loads a {@code List<ArticleItem>}
 * containing all installed applications on the device.
 */
public abstract class ListLoader<E> extends AsyncTaskLoader<List<E>> {
	protected static final String TAG = "ListLoader";
	protected static final boolean DEBUG = false;

	// We hold a reference to the Loader�s data here.
	private List<E> mData;

	public ListLoader(Context ctx) {
		// Loaders may be used across multiple Activitys (assuming they aren't
		// bound to the LoaderManager), so NEVER hold a reference to the context
		// directly. Doing so will cause you to leak an entire Activity's
		// context.
		// The superclass constructor will store a reference to the Application
		// Context instead, and can be retrieved with a call to getContext().
		super(ctx);
	}

	/****************************************************/
	/** (1) A task that performs the asynchronous load **/
	/****************************************************/

	/**
	 * This method is called on a background thread and generates a List of
	 * {@link E} objects. Each entry corresponds to a single installed
	 * application on the device.
	 */
	@Override
	public abstract List<E> loadInBackground();

	/*******************************************/
	/** (2) Deliver the results to the client **/
	/*******************************************/

	/**
	 * Called when there is new data to deliver to the client. The superclass
	 * will deliver it to the registered listener (i.e. the LoaderManager),
	 * which will forward the results to the client through a call to
	 * onLoadFinished.
	 */
	@Override
	public void deliverResult(List<E> items) {
		if (isReset()) {
			if (DEBUG)
				Log.w(TAG,
                    "+++ Warning! An async query came in while the Loader was reset! +++");
			// The Loader has been reset; ignore the result and invalidate the
			// data.
			// This can happen when the Loader is reset while an asynchronous
			// query
			// is working in the background. That is, when the background thread
			// finishes its work and attempts to deliver the results to the
			// client,
			// it will see here that the Loader has been reset and discard any
			// resources associated with the new data as necessary.
			if (items != null) {
				releaseResources(items);
				return;
			}
		}

		// Hold a reference to the old data so it doesn't get garbage collected.
		// We must protect it until the new data has been delivered.
		List<E> oldApps = mData;
		mData = items;

		if (isStarted()) {
			if (DEBUG)
				Log.i(TAG, "+++ Delivering results to the LoaderManager for"
                    + " the ListFragment to display! +++");
			// If the Loader is in a started state, have the superclass deliver
			// the
			// results to the client.
			super.deliverResult(items);
		}

		// Invalidate the old data as we don't need it any more.
		if (oldApps != null && oldApps != items) {
			if (DEBUG)
				Log.i(TAG,
                    "+++ Releasing any old data associated with this Loader. +++");
			releaseResources(oldApps);
		}
	}

	/*********************************************************/
	/** (3) Implement the Loader�s state-dependent behavior **/
	/*********************************************************/

	@Override
	protected void onStartLoading() {
		if (DEBUG)
			Log.i(TAG, "+++ onStartLoading() called! +++");

		if (mData != null) {
			// Deliver any previously loaded data immediately.
			if (DEBUG)
				Log.i(TAG,
                    "+++ Delivering previously loaded data to the client...");
			deliverResult(mData);
		}

		if (takeContentChanged()) {
			// When the observer detects a new installed application, it will
			// call
			// onContentChanged() on the Loader, which will cause the next call
			// to
			// takeContentChanged() to return true. If this is ever the case (or
			// if
			// the current data is null), we force a new load.
			if (DEBUG)
				Log.i(TAG,
                    "+++ A content change has been detected... so force load! +++");
			forceLoad();
		} else if (mData == null) {
			// If the current data is null... then we should make it non-null!
			// :)
			if (DEBUG)
				Log.i(TAG,
                    "+++ The current data is data is null... so force load! +++");
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		if (DEBUG)
			Log.i(TAG, "+++ onStopLoading() called! +++");

		// The Loader has been put in a stopped state, so we should attempt to
		// cancel the current load (if there is one).
		cancelLoad();

		// Note that we leave the observer as is; Loaders in a stopped state
		// should still monitor the data source for changes so that the Loader
		// will know to force a new load if it is ever started again.
	}

	@Override
	protected void onReset() {
		if (DEBUG)
			Log.i(TAG, "+++ onReset() called! +++");

		// Ensure the loader is stopped.
		onStopLoading();

		// At this point we can release the resources associated with 'apps'.
		if (mData != null) {
			releaseResources(mData);
			mData = null;
		}

		// The Loader is being reset, so we should stop monitoring for changes.
		// if (mAppsObserver != null) {
		// getContext().unregisterReceiver(mAppsObserver);
		// mAppsObserver = null;
		// }
		//
		// if (mLocaleObserver != null) {
		// getContext().unregisterReceiver(mLocaleObserver);
		// mLocaleObserver = null;
		// }
	}

	@Override
	public void onCanceled(List<E> apps) {
		if (DEBUG)
			Log.i(TAG, "+++ onCanceled() called! +++");

		// Attempt to cancel the current asynchronous load.
		super.onCanceled(apps);

		// The load has been canceled, so we should release the resources
		// associated with 'mApps'.
		releaseResources(apps);
	}

	@Override
	public void forceLoad() {
		if (DEBUG)
			Log.i(TAG, "+++ forceLoad() called! +++");
		super.forceLoad();
	}

	/**
	 * Helper method to take care of releasing resources associated with an
	 * actively loaded data set.
	 */
	private void releaseResources(List<E> mData) {
		// For a simple List, there is nothing to do. For something like a
		// Cursor,
		// we would close it in this method. All resources associated with the
		// Loader should be released here.
	}

	/*********************************************************************/
	/** (4) Observer which receives notifications when the data changes **/
	/*********************************************************************/

	/**************************/
	/** (5) Everything else! **/
	/**************************/
	


}
