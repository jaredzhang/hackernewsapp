package com.jaredzhang.hackersnewsapp.model;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.jaredzhang.hackersnewsapp.R;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhuodong on 2/7/15.
 */
public class Item implements Parcelable,Serializable{

//    public static enum ItemType{
//
//    }


    int id;
    int parent;
    boolean deleted;
    String type;
    String by;
    long time;
    String text;
    boolean dead;
    int[] kids;
    String url;
    int score;
    String title;
    List<Item> parts;

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public String getType() {
        return type;
    }

    public String getBy() {
        return by;
    }

    public long getTime() {
        return time;
    }

    public String getText() {
        return text;
    }

    public boolean isDead() {
        return dead;
    }

    public int getParent() {
        return parent;
    }

    public int[] getKids() {
        return kids;
    }

    public String getUrl() {
        return url;
    }

    public int getScore() {
        return score;
    }

    public List<Item> getParts() {
        return parts;
    }

    String formattedTime;
    String formattedSubtitle;

    public String getFormattedTime() {
        if (TextUtils.isEmpty(formattedTime)) {
            formattedTime = DateUtils.getRelativeTimeSpanString(time * 1000, System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        }
        return formattedTime;
    }

    public String getFormattedSubTitle(Resources resources) {
        if (TextUtils.isEmpty(formattedSubtitle)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(resources.getQuantityString(R.plurals.points, score, score)).append(' ')
                .append(resources.getString(R.string.by, by)).append('\n')
                .append(getFormattedTime()).append('\n');

            if (kids != null && kids.length > 0) {
                stringBuilder.append(resources.getQuantityString(R.plurals.comments, kids.length, kids.length));
            }
            formattedSubtitle = stringBuilder.toString();
        }
        return formattedSubtitle;
    }

    boolean empty;

    public Item(boolean empty) {
        this.empty = empty;
    }

    public boolean isEmpty() {
        return empty;
    }

    public Item() {
    }

    Item latestChild;

    public Item getLatestChild() {
        return latestChild;
    }

    public void setLatestChild(Item latestChild) {
        this.latestChild = latestChild;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.type);
        dest.writeString(this.by);
        dest.writeLong(this.time);
        dest.writeString(this.text);
        dest.writeIntArray(this.kids);
        dest.writeString(this.url);
        dest.writeInt(this.score);
        dest.writeString(this.title);
    }

    private Item(Parcel in) {
        this.id = in.readInt();
        this.type = in.readString();
        this.by = in.readString();
        this.time = in.readLong();
        this.text = in.readString();
        this.kids = in.createIntArray();
        this.url = in.readString();
        this.score = in.readInt();
        this.title = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}
