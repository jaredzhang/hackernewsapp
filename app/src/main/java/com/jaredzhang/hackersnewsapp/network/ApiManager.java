package com.jaredzhang.hackersnewsapp.network;

import com.jaredzhang.hackersnewsapp.BuildConfig;
import com.jaredzhang.hackersnewsapp.model.Item;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;


/**
 * Created by zhuodong on 3/10/14.
 */
public final class ApiManager {

    private static ApiService mApiService;

    private ApiManager() {
    }

    public static ApiService getApiClient() {
        if (mApiService == null) {

            OkHttpClient mOkHttpClient = new OkHttpClient();
            mOkHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
            mOkHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

            RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ApiConfig.BAST_HOST)
                .setRequestInterceptor(new CustomRequestInterceptor())
                //.setErrorHandler(new MyErrorHandler())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC
                    : RestAdapter.LogLevel.NONE)
                .setClient(new OkClient(mOkHttpClient))
                .build();

            mApiService = restAdapter.create(ApiService.class);
        }
        return mApiService;
    }

    public interface ApiService {

        @GET(ApiConfig.EndPoint.TOP_STORIES)
        Integer[] getTopStories();

        @GET(ApiConfig.EndPoint.ITEM)
        Observable<Item> getItem(@Path("id") int itemId);

        @GET(ApiConfig.EndPoint.ITEM)
        Item getChild(@Path("id") int itemId);

    }

    private static final class CustomRequestInterceptor implements RequestInterceptor {

        private CustomRequestInterceptor() {
        }

        @Override
        public void intercept(RequestFacade request) {
            request.addQueryParam("print","pretty");
        }
    }

//    final static class MyErrorHandler implements ErrorHandler {
//
//        @Override
//        public Throwable handleError(RetrofitError cause) {
//
//            if (cause.getResponse() != null && cause.getResponse().getStatus() == 401) {
//                EventBus.getDefault().post(new UnauthorizedEvent(EventStatus.SUCCEED));
//            }
//            return cause;
//        }
//    }

}
