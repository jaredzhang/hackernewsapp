package com.jaredzhang.hackersnewsapp.network;


import com.jaredzhang.hackersnewsapp.BuildConfig;

/**
 * Created by zhuodong on 11/9/14.
 */
public class ApiConfig {

    private ApiConfig() {
    }

    public static class EndPoint {
        public static final String ITEM = "/v0/item/{id}.json";
        public static final String TOP_STORIES = "/v0/topstories.json";


    }

    public static final String BAST_HOST =
        BuildConfig.DEBUG?"http://hacker-news.firebaseio.com":"https://hacker-news.firebaseio.com";

}
