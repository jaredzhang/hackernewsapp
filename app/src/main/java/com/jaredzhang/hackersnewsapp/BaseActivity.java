package com.jaredzhang.hackersnewsapp;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import de.greenrobot.event.EventBus;

public abstract class BaseActivity extends ActionBarActivity {

    public boolean isAlive, isPaused, subscribedEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAlive = true;
        subscribedEvent = isSubscribedEvent();
        if (subscribedEvent) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        isAlive = false;
        if (subscribedEvent) {
            EventBus.getDefault().unregister(this);
        }
    }

    public boolean isSubscribedEvent() {
        return false;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == android.R.id.home) {
//            finish();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    protected boolean hasBack(){return true;}
}
