package com.jaredzhang.hackersnewsapp.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaredzhang.hackersnewsapp.R;


public class PagingRecyclerView extends RecyclerView {

    private boolean isLoading;
    private boolean hasMoreItems;
    private Pagingnable pagingnableListener;
    private View loadingView;
    boolean isTopLoading;
    private OnScrollListener onScrollListener;
    int loadingLayout;


    public PagingRecyclerView(Context context) {
        this(context, null);
    }

    public PagingRecyclerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PagingRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PagingRecyclerView);
        loadingLayout = a.getResourceId(R.styleable.PagingRecyclerView_loadingLayout, R.layout.layout_loading_common);
        isTopLoading = a.getBoolean(R.styleable.PagingRecyclerView_isTopLoading, false);
        a.recycle();


        isLoading = false;

        loadingView = LayoutInflater.from(getContext()).inflate(loadingLayout, null);

        super.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (onScrollListener != null) {
                    onScrollListener.onScrollStateChanged(recyclerView, newState);
                }
//                Log.d(""," testing onScrollStateChanged ");

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (onScrollListener != null) {
                    onScrollListener.onScrolled(recyclerView, dx, dy);
                }
                LinearLayoutManager layoutManager = (LinearLayoutManager) getLayoutManager();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

//                Log.d(""," testing visibleItemCount "+visibleItemCount);
//                Log.d(""," testing totalItemCount "+totalItemCount);
//                Log.d(""," testing firstVisibleItem "+firstVisibleItem);

                if (totalItemCount > 0) {
                    int lastVisibleItem = firstVisibleItem + visibleItemCount;
                    if (!isLoading && hasMoreItems && (isTopLoading ? firstVisibleItem == 0 : (lastVisibleItem == totalItemCount))) {
                        if (pagingnableListener != null) {
                            isLoading = true;
                            pagingnableListener.onLoadMoreItems();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void setAdapter(Adapter adapter) {
        HeaderAdapter headerAdapter = new HeaderAdapter(adapter);
        headerAdapter.setHasMoreItems(hasMoreItems);
        super.setAdapter(headerAdapter);
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setPagingnableListener(Pagingnable pagingnableListener) {
        this.pagingnableListener = pagingnableListener;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
        if (getAdapter() instanceof HeaderAdapter) {
            ((HeaderAdapter) getAdapter()).setHasMoreItems(hasMoreItems);
        }
    }

    public boolean hasMoreItems() {
        return this.hasMoreItems;
    }

    public void onFinishLoading(boolean hasMoreItems) {
        setHasMoreItems(hasMoreItems);
        setIsLoading(false);
    }

    @Override
    public void setOnScrollListener(OnScrollListener listener) {
        onScrollListener = listener;
    }


//    @Override
//    public boolean canScrollVertically(int direction) {
//        // check if scrolling up
//        if (direction < 1) {
//            boolean original = super.canScrollVertically(direction);
//            return !original && getChildAt(0) != null && getChildAt(0).getTop() < 0 || original;
//        }
//        return super.canScrollVertically(direction);
//    }

    public static class HeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private static final int TYPE_HEADER = 0;
        private static final int TYPE_ITEM = 1;
        Adapter<RecyclerView.ViewHolder> baseAdapter;
        boolean hasMoreItems;

        public HeaderAdapter(Adapter baseAdapter) {
            this.baseAdapter = baseAdapter;
        }

        public void setHasMoreItems(boolean hasMoreItems) {
            this.hasMoreItems = hasMoreItems;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == TYPE_HEADER) {
                //inflate your layout and pass it to view holder
                View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_loading_common, parent, false);
                return new VHHeader(view);
            } else if (viewType == TYPE_ITEM) {
                //inflate your layout and pass it to view holder
                return baseAdapter.onCreateViewHolder(parent, viewType);
            }

            throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof VHHeader) {
                //cast holder to VHHeader and set data for header.
            } else {
                baseAdapter.onBindViewHolder(holder, position);
            }
        }

        @Override
        public int getItemCount() {
            return baseAdapter.getItemCount() + (hasMoreItems?1:0);
        }

        @Override
        public int getItemViewType(int position) {
            if (hasMoreItems && isPositionHeader(position))
                return TYPE_HEADER;

            return TYPE_ITEM;
        }

        private boolean isPositionHeader(int position) {
            return position == getItemCount() - 1;
        }

        class VHHeader extends RecyclerView.ViewHolder {
            public VHHeader(View itemView) {
                super(itemView);
            }
        }
    }
}
