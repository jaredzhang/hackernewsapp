package com.jaredzhang.hackersnewsapp.service;

import android.app.IntentService;
import android.content.Intent;

import com.jaredzhang.hackersnewsapp.event.EventStatus;
import com.jaredzhang.hackersnewsapp.event.GetCommentsEvent;
import com.jaredzhang.hackersnewsapp.helper.CacheManager;
import com.jaredzhang.hackersnewsapp.helper.Const;
import com.jaredzhang.hackersnewsapp.helper.LastUpdateTimeDataHelper;
import com.jaredzhang.hackersnewsapp.model.Item;
import com.jaredzhang.hackersnewsapp.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import rx.Observable;
import rx.Observer;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class GetStoryCommentService extends IntentService {
    public GetStoryCommentService() {
        super(GetStoryCommentService.class.getSimpleName());
    }

    public static final String TAG = GetStoryCommentService.class.getSimpleName();

    CacheManager cacheManager = CacheManager.getInstance();


    @Override
    protected void onHandleIntent(Intent intent) {

        int pageSize = intent.getIntExtra(Const.INTENT_EXTRA_PAGE_SIZE, 0);
        int pageNo = intent.getIntExtra(Const.INTENT_EXTRA_PAGE_NO, 0);
        boolean refresh = intent.getBooleanExtra(Const.INTENT_EXTRA_REFRESH, false);
        int[] commentIds = intent.getIntArrayExtra(Const.INTENT_EXTRA_COMMENT_IDS);
        int parentItemId = intent.getIntExtra(Const.INTENT_EXTRA_ITEM_ID, 0);

        if (commentIds == null) return;

        //Timber.d(" stories ids %s", Arrays.toString(ids));

        //if size equals zero, return all
        if (pageSize == 0 || pageNo == 0) {
            pageNo = 1;
            pageSize = commentIds.length;
        }

        final String commentCacheKey = CacheManager.getItemCacheKey(parentItemId);
        final boolean cacheExpired = LastUpdateTimeDataHelper.isExpired(commentCacheKey);
        if (!refresh && !cacheExpired) {

            return;
        }

        //final boolean moreItems = pageSize * pageNo < commentIds.length;
        List<Integer> itemIds = new ArrayList<>();
        for (int i = pageSize * (pageNo - 1); i < Math.min(commentIds.length, pageSize * pageNo); i++) {
            if (i >= 0 && i < commentIds.length) {
                itemIds.add(commentIds[i]);
            }
        }

        Observable.from(itemIds).flatMap(new Func1<Integer, Observable<Item>>() {
            @Override
            public Observable<Item> call(Integer integer) {
                return ApiManager.getApiClient().getItem(integer);
            }
        }).onErrorReturn(new Func1<Throwable, Item>() {
            @Override
            public Item call(Throwable throwable) {
                return new Item(true);
            }
        }).observeOn(Schedulers.trampoline())
            .flatMap(new Func1<Item, Observable<Item>>() {
                @Override
                public Observable<Item> call(Item item) {
                    if (item.getKids() != null && item.getKids().length > 0) {
                        Item item1 = ApiManager.getApiClient().getChild(item.getKids()[0]);
                        item.setLatestChild(item1);
                    }
                    return Observable.from(item);
                }
            })
            .subscribe(new Observer<Item>() {
                @Override
                public void onCompleted() {
                    LastUpdateTimeDataHelper.saveTimeStamp(commentCacheKey);
                    EventBus.getDefault().post(new GetCommentsEvent(EventStatus.SUCCEED));
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(Item item) {
                    if (!item.isEmpty()) {
                        cacheManager.put(CacheManager.getCommentCacheKey(item.getId()), item);
                    }
                }
            });

    }


}
