package com.jaredzhang.hackersnewsapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.SparseIntArray;

import com.jaredzhang.hackersnewsapp.event.EventStatus;
import com.jaredzhang.hackersnewsapp.event.GetTopStoriesEvent;
import com.jaredzhang.hackersnewsapp.helper.CacheManager;
import com.jaredzhang.hackersnewsapp.helper.Const;
import com.jaredzhang.hackersnewsapp.helper.LastUpdateTimeDataHelper;
import com.jaredzhang.hackersnewsapp.helper.Utils;
import com.jaredzhang.hackersnewsapp.model.Item;
import com.jaredzhang.hackersnewsapp.network.ApiConfig;
import com.jaredzhang.hackersnewsapp.network.ApiManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;
import rx.Observable;
import rx.Observer;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class UpdateTopStoriesService extends IntentService {
    public UpdateTopStoriesService() {
        super(UpdateTopStoriesService.class.getSimpleName());
    }

    public static final String TAG = UpdateTopStoriesService.class.getSimpleName();

    CacheManager cacheManager = CacheManager.getInstance();

    @Override
    protected void onHandleIntent(Intent intent) {

        int pageSize = intent.getIntExtra(Const.INTENT_EXTRA_PAGE_SIZE, 0);
        int pageNo = intent.getIntExtra(Const.INTENT_EXTRA_PAGE_NO, 0);
        boolean refresh = intent.getBooleanExtra(Const.INTENT_EXTRA_REFRESH, false);

        String topStoryCacheKey = Utils.md5(ApiConfig.EndPoint.TOP_STORIES);
        Integer[] ids = cacheManager.getArray(topStoryCacheKey, Integer.class);

        Timber.d(" stories ids %s", Arrays.toString(ids));
        Timber.d(" cache expires %s", LastUpdateTimeDataHelper.isExpired(topStoryCacheKey));

        if (refresh || LastUpdateTimeDataHelper.isExpired(topStoryCacheKey) || ids == null) {
            //clean previous topstories;
            cacheManager.delete(CacheManager.ITEM_BASE_KEY);
            cacheManager.delete(CacheManager.COMMENTS_BASE_KEY);
            LastUpdateTimeDataHelper.clearAll();

            try {
                ids = ApiManager.getApiClient().getTopStories();
                LastUpdateTimeDataHelper.saveTimeStamp(topStoryCacheKey);
            } catch (RetrofitError error) {
                EventBus.getDefault().post(new GetTopStoriesEvent(EventStatus.FAILED));
                return;
            }

            cacheManager.put(topStoryCacheKey, ids);
        }

        //Timber.d(" stories ids %s", Arrays.toString(ids));

        //if size equals zero, return all
        if (pageSize == 0 || pageNo == 0) {
            pageNo = 1;
            pageSize = ids.length;
        }
        String tempCacheKey;
        Item tempItem;
        final SparseIntArray missingItemsMap = new SparseIntArray();

        for (int i = pageSize * (pageNo - 1), index = 0; i < pageSize * pageNo; i++, index++) {
            if (i >= 0 && i < ids.length) {
                tempCacheKey = CacheManager.getItemCacheKey(ids[i]);
                tempItem = cacheManager.get(tempCacheKey, Item.class);
                if (tempItem == null) {
                    missingItemsMap.put(ids[i], index);
                    //Timber.d(" put missing item id %d  index %d", ids[i], index);
                }
            }
        }

        if(missingItemsMap.size() == 0) {
            EventBus.getDefault().post(new GetTopStoriesEvent(EventStatus.SUCCEED));
            return;
        }

        List<Integer> itemIds = new ArrayList<>();
        for (int i1 = 0; i1 < missingItemsMap.size(); i1++) {
            itemIds.add(missingItemsMap.keyAt(i1));
        }
        Observable.from(itemIds).flatMap(new Func1<Integer, Observable<Item>>() {
            @Override
            public Observable<Item> call(Integer integer) {
                return ApiManager.getApiClient().getItem(integer);
            }
        }).onErrorReturn(new Func1<Throwable, Item>() {
            @Override
            public Item call(Throwable throwable) {
                return new Item(true);
            }
        }).observeOn(Schedulers.trampoline())
            .subscribe(new Observer<Item>() {
                @Override
                public void onCompleted() {
                    EventBus.getDefault().post(new GetTopStoriesEvent(EventStatus.SUCCEED));
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onNext(Item item) {
                    if(!item.isEmpty()) {
                        cacheManager.put(CacheManager.getItemCacheKey(item.getId()), item);
                    }
                }
            });
    }


}
