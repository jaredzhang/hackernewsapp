package com.jaredzhang.hackersnewsapp.event;


/**
 * Created by zhuodong on 2/3/14.
 */
public class BaseEvent<T> {
    EventStatus status;
    T response;

    public BaseEvent(EventStatus status) {
        this.status = status;
    }

    public BaseEvent(EventStatus status, T response) {
        this.status = status;
        this.response = response;
    }

    public EventStatus getStatus() {
        return status;
    }

    String errorMsg;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public boolean isSucceed() {
        return status == EventStatus.SUCCEED;
    }
}
