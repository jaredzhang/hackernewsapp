package com.jaredzhang.hackersnewsapp.event;

/**
 * Created by zhuodong on 2/7/15.
 */
public class GetCommentsEvent extends BaseEvent{

    boolean reload;

    public GetCommentsEvent(EventStatus status) {
        super(status);
    }

    public GetCommentsEvent(EventStatus status, boolean reload) {
        super(status);
        this.reload = reload;
    }
}
