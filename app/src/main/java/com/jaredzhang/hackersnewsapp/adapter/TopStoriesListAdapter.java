package com.jaredzhang.hackersnewsapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jaredzhang.hackersnewsapp.HackerNewsApplication;
import com.jaredzhang.hackersnewsapp.R;
import com.jaredzhang.hackersnewsapp.model.Item;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zhuodong on 11/5/14.
 */
public class TopStoriesListAdapter extends RecyclerView.Adapter<TopStoriesListAdapter.ViewHolder> {

    List<Item> items;
    OnItemClickListener mItemClickListener;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.item_top_news, viewGroup, false);
        return new ViewHolder(view, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.render(items.get(i));
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        @InjectView(R.id.tv_item_title)
        TextView mTvItemTitle;
        @InjectView(R.id.tv_item_subtitle)
        TextView mTvItemSubtitle;
        @InjectView(R.id.btn_view_web)
        Button mBtnViewWeb;
        View view;

        Item item;

        OnItemClickListener onItemClickListener;

        StringBuilder sb;

        public ViewHolder(View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            this.view = itemView;
            this.onItemClickListener = onItemClickListener;
            ButterKnife.inject(this, itemView);
            view.setOnClickListener(this);
            mBtnViewWeb.setOnClickListener(this);
        }


        public void render(Item item) {
            this.item = item;

            mTvItemTitle.setText(item.getTitle());
            if(TextUtils.isEmpty(item.getUrl())) {
                mBtnViewWeb.setVisibility(View.GONE);
            }else {
                mBtnViewWeb.setText(R.string.view_in_web);
                //mBtnViewWeb.setText(HackerNewsApplication.getInstance().getString(R.string.view_in_web, item.getUrl()));
            }

            mTvItemSubtitle.setText(item.getFormattedSubTitle(HackerNewsApplication.getInstance().getResources()));
         }


        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                if (v == mBtnViewWeb) {
                    onItemClickListener.onViewOnWebClicked(v, getPosition(), item);
                } else if (v == view) {
                    onItemClickListener.onItemClick(v, getPosition(), item);
                }
            }
        }
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void addItems(List<Item> items) {
        if(this.items == null) {
            this.items = new ArrayList<>();
        }
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, Item item);
        public void onViewOnWebClicked(View view, int position, Item item);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}
