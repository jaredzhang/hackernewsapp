package com.jaredzhang.hackersnewsapp.adapter;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaredzhang.hackersnewsapp.R;
import com.jaredzhang.hackersnewsapp.model.Item;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zhuodong on 11/5/14.
 */
public class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.ViewHolder> {

    List<Item> items;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.item_comment, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        if(items != null) {
            viewHolder.render(items.get(i));
        }
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        @InjectView(R.id.tv_comment_title)
        TextView mTvCommentTitle;
        @InjectView(R.id.tv_comment_content)
        TextView mTvCommentContent;
        @InjectView(R.id.tv_subcomment_title)
        TextView mTvSubcommentTitle;
        @InjectView(R.id.tv_subcomment_content)
        TextView mTvSubcommentContent;
        View view;

        Item item;

        public ViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            ButterKnife.inject(this, itemView);
        }


        public void render(Item item) {
            this.item = item;

            if(item.isDeleted()) {
                mTvCommentContent.setTypeface(mTvCommentContent.getTypeface(), Typeface.ITALIC);
                mTvCommentContent.setText(R.string.comment_deleted);
                mTvCommentTitle.setText("");
            }else {
                mTvCommentContent.setTypeface(mTvCommentContent.getTypeface(), Typeface.NORMAL);
                if (!TextUtils.isEmpty(item.getText())) {
                    mTvCommentContent.setText(Html.fromHtml(item.getText()));
                }
                mTvCommentTitle.setText(item.getBy() + " " + item.getFormattedTime());
            }

            Item child = item.getLatestChild();
            if(child != null && !child.isDeleted()) {
                mTvSubcommentContent.setVisibility(View.VISIBLE);
                mTvSubcommentTitle.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(child.getText())) {
                    mTvSubcommentContent.setText(Html.fromHtml(child.getText()));
                }
                mTvSubcommentTitle.setText(child.getBy()+" "+child.getFormattedTime());
            }else {
                mTvSubcommentContent.setVisibility(View.GONE);
                mTvSubcommentTitle.setVisibility(View.GONE);
            }
        }
    }

    public void setItems(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

}
